
#Taller 16: Tienda de Discos

class Albumes:

    def __init__(nombre, numeroDiscos, genero, disponibilidad):
        self.nombre=nombre
        self.numeroDiscos=numeroDiscos
        self.genero=genero
        self.disponibilidad=disponibilidad

    def consultarDisponibilidad(self):
        if self.disponibilidad>1:
            print("Está disponible el álbum", self.nombre)
        else:
            print("No está dispoible el álbum", self.nombre)
    
    def crearÁlbum(self):
        print("Ha sido creado el álbum", self.nombre)
    def borrarÁlbum(self):
        print("Ha sido eliminado el álbum", self.nombre)
    def actualizarÁlbum(self):
        print("Ha sido actualizado el álbum", self.nombre)
    def seleccionarÁlbum(self):
        print("Ha sido seleccionado el álbum", self.nombre)


class Discos:

    def __init__(precio, genero, marca, ID):
        self.precio=precio
        self.genero=genero
        self.marca=marca
        self.ID=[]

    def consultarDisco(self):
        print("Ha sido eliminado el disco", self.nombre)
    def crearDisco(self):
        print("Ha sido creado el disco", self.nombre)
    def borrarDisco(self):
        print("Ha sido eliminado el disco", self.nombre)
    def actualizarDisco(self):
        print("Ha sido actualizado el disco", self.nombre)
    def seleccionarDisco(self):
        print("Ha sido seleccionado el disco", self.nombre)


class Proveedores:

    def __init__(nombre, documento, direccion, correo, telefono):

        self.nombre=nombre
        self.documento=documento 
        self.direccion=direccion
        self.correo=[]
        self.telefono=telefono

    def ingresar(self):
        print("Ha sido ingresado al sistema el proveedor", self.nombre)
    def modificar(self):
        print("Ha sido modificado el proveedor", self.nombre)
    def consultar(self):
        print("La información de el proveedor", self.nombre)
    def crearProveedor(self):
        print("Ha sido creado el proveedor", self.nombre)
    def borrarProveedor(self):
        print("Ha sido eliminado el proveedor", self.nombre)
    def actualizarProveedor(self):
        print("Ha sido actualizado el proveedor", self.nombre)

class Compradores:

    def __init__(nombre, documento, direccion, correo, telefono):

        self.nombre=nombre
        self.documento=documento 
        self.direccion=direccion
        self.correo=[]
        self.telefono=telefono

    def ingresar(self):
        print("Ha sido ingresado al sistema el comprador", self.nombre)
    def modificar(self):
        print("Ha sido modificado el comprador", self.nombre)
    def consultar(self):
        print("La información de el comprador", self.nombre)
    def crearComprador(self):
        print("Ha sido creado el comprador", self.nombre)
    def borrarComprador(self):
        print("Ha sido eliminado el comprador", self.nombre)
    def actualizarComprador(self):
        print("Ha sido actualizado el comprador", self.nombre)







